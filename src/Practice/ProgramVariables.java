
package Practice;


public class ProgramVariables {
    public static void main(String[] args) {
        //eight variables
        // datatype    variableName =  Value ;
        
        byte b = 127;//126 to -127
        short s = 255;
        int i = 4567890; // integer values
        long l = 3245671893L; // using L
        float f = 456.781F; //using F
        double d = 3456789.45667;
        boolean bool = true; // true & false
        char c = 'A';
        
        String str = "Lucifer";
        str = "lucifer loves chloe";
        
        System.out.println(b);
        System.out.println(s);
        System.out.println(i);
        System.out.println(l);
        System.out.println(f);
        System.out.println(f);
        System.out.println(d);
        System.out.println(bool);
        System.out.println(c);
        System.out.println(str);
        
        
    }
}
