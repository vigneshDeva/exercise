
package basics;

 class Logical {
     public static void main(String[] args) {
         boolean m = (true && (12 > (16 - 1)));
         System.out.println(m);
         
         boolean a = false&&false;
         boolean b = false&&true;
         boolean c = true&&false;
         boolean d = true&&true;
         
         System.out.println(a);
         
         boolean e = ((12+23)*(20-11)>(24%2)+(13-8));
         System.out.println(e);
         
         int g = 12;
         int h = 25;
         
         System.out.println((g>h)||(g>h));
     }
}
